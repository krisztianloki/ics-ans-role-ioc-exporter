# ics-ans-role-ioc-exporter

Ansible role to install IOC-related Prometheus exporters.

Will parse `/opt/iocs` for valid IOCs and export:
- various (boolean) metrics about the IOC/IOC setup
- select PVs from the IOCs (should these PVs exist)

## Role Variables

```yaml
ioc_prometheus_exporter_directory: /opt/prometheus
ioc_prometheus_exporter_user: prometheus
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ioc
```

## License

BSD 2-clause
