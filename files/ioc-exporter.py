"""
A Python2 Prometheus exporter for IOC metadata

This script sets up a webserver that exports to Prometheus data about an IOC state.
Checks if:
- there have been local modifications made
- usage of standard utilities is correct
"""

import json
import logging
import logging.handlers
import os
import subprocess
import time

import click
import git
import yaml
from prometheus_client import start_http_server
from prometheus_client.core import REGISTRY, GaugeMetricFamily
from pvmonitor import PVMonitor

IOC_CLONE_DIR = "/opt/iocs"
IOC_METADATA_YAML_FILE = "ioc.yml"
IOC_METADATA_JSON_FILE = "ioc.json"
DEPLOYMENT_METADATA_FILE = ".deployment/metadata.yml"
LOGFILE_NAME = "/var/log/prometheus/ioc-exporter.log"
MAX_LOGFILE_SIZE = 50 * 1024 * 1024  # 50MB
LOGFILE_BACKUP_COUNT = 1


class IOCCollector(PVMonitor):
    def __init__(self, iocdirs, logger):
        super(IOCCollector, self).__init__(logger)
        self.iocdirs = iocdirs

    def collect(self):
        ioc_dirs = GaugeMetricFamily(
            "ioc_dirs",
            "Number of directories in /opt/iocs",
        )
        git_dirty = GaugeMetricFamily(
            "ioc_repository_dirty",
            "e3 IOC git repository has non-commited local changes",
            labels=["ioc"],
        )
        git_diff = GaugeMetricFamily(
            "ioc_repository_local_commits",
            "e3 IOC git repository has local committed changes",
            labels=["ioc"],
        )
        service_disabled = GaugeMetricFamily(
            "ioc_service_disabled",
            "e3 IOC system daemon is disabled",
            labels=["ioc"],
        )
        essioc_missing = GaugeMetricFamily(
            "ioc_essioc_missing",
            "e3 IOC does not use essioc",
            labels=["ioc"],
        )
        cellmode_active = GaugeMetricFamily(
            "ioc_cellmode_used",
            "e3 IOC uses cellmode",
            labels=["ioc"],
        )
        ioc_dirs.add_metric([], len(self.iocdirs))
        for iocdir in self.iocdirs:
            try:
                with open(
                    os.path.join(os.path.abspath(iocdir), DEPLOYMENT_METADATA_FILE), "r"
                ) as config_file:
                    try:
                        ioc = yaml.safe_load(config_file)
                    except yaml.YAMLError as error:
                        self.logger.error(
                            "Failure loading config file {}: {}".format(
                                config_file, error
                            )
                        )
                        continue
                    else:
                        ioc["dir"] = iocdir
                        _run_dir = "/run/ioc@{}".format(os.path.basename(iocdir))
                        if os.path.isdir(_run_dir):
                            self.subscribe_to_pvs(
                                ["{}:{}".format(ioc["name"], "essioc_VER")]
                            )
                        try:
                            self.check_ioc_daemon_enabled(ioc, service_disabled)
                        except Exception as error:
                            self.logger.exception(error)
                        try:
                            self.check_essioc_usage(ioc, essioc_missing)
                        except Exception as error:
                            self.logger.exception(error)
                        try:
                            self.check_cellmode_usage(ioc, cellmode_active)
                        except Exception as error:
                            self.logger.exception(error)
                        try:
                            self.check_git_state(ioc, git_dirty, git_diff)
                        except Exception as error:
                            self.logger.exception(error)
            except Exception as error:
                self.logger.exception(error)
        yield ioc_dirs
        yield git_dirty
        yield git_diff
        yield service_disabled
        yield essioc_missing
        yield cellmode_active

    def __parse_ioc_yml(self, ioc):
        metadata_fpath = os.path.join(ioc["dir"], IOC_METADATA_YAML_FILE)
        with open(metadata_fpath) as f:
            try:
                return yaml.safe_load(f)
            except yaml.YAMLError as error:
                self.logger.error(
                    "Failure loading metadata file {}: {}".format(metadata_fpath, error)
                )

    def __parse_ioc_json(self, ioc):
        metadata_fpath = os.path.join(ioc["dir"], IOC_METADATA_JSON_FILE)
        with open(metadata_fpath) as f:
            try:
                return json.load(f)
            except Exception as error:
                self.logger.error(
                    "Failure loading metadata file {}: {}".format(metadata_fpath, error)
                )

    def check_cellmode_usage(self, ioc, metric):
        """Exports boolean indicating whether cellmode is active."""
        _cellmode_active = True  # assume the worst
        try:
            metadata = self.__parse_ioc_yml(ioc)
        except IOError as error:
            if error.errno == 2:
                metadata = self.__parse_ioc_json(ioc)
            else:
                self.logger.error("Failure loading metadata file: {}".format(error))
        if metadata is not None:
            try:
                _cellmode_active = bool(
                    metadata["cells"]
                )  # we only care if anything is set
            except KeyError:
                _cellmode_active = False  # cellmode is inactive if cell-list is empty
        metric.add_metric([ioc["name"]], int(_cellmode_active))

    def check_git_state(self, ioc, metric_dirty, metric_diff):
        """Exports booleans indicating if local and remote git repositories are out of sync."""
        try:
            repo = git.Repo(ioc["dir"])
        except git.InvalidGitRepositoryError:
            self.logger.error(
                "Failure trying to identify git repository: {}".format(ioc["dir"])
            )
        else:
            metric_dirty.add_metric(
                [ioc["name"]], int(repo.is_dirty(untracked_files=True))
            )
            deployed_version = repo.git.rev_parse(
                "{}^{{commit}}".format(ioc["version"])
            )
            current_version = repo.git.rev_parse(repo.commit().hexsha)
            metric_diff.add_metric(
                [ioc["name"]], int(deployed_version != current_version)
            )

    def check_ioc_daemon_enabled(self, ioc, metric):
        """Exports boolean showing if IOC service is disabled."""
        ioc_service_name = "ioc@{}.service".format(ioc["name"])
        ioc_service_unitfilestate = subprocess.check_output(
            [
                "systemctl",
                "show",
                "--no-pager",
                ioc_service_name,
                "--property=UnitFileState",
            ],
            universal_newlines=True,
        ).strip()
        metric.add_metric(
            [ioc["name"]], int(ioc_service_unitfilestate.endswith("disabled"))
        )

    def check_essioc_usage(self, ioc, metric):
        """Exports boolean indicating if essioc is not loaded."""
        _essioc_missing = self.get_pv("{}:{}".format(ioc["name"], "essioc_VER")) is None
        metric.add_metric([ioc["name"]], _essioc_missing)


@click.command()
@click.option("--port", default=12110, help="TCP port the exporter will listen on")
@click.option(
    "--path",
    default=IOC_CLONE_DIR,
    type=click.Path(),
    help="Root directory for IOCs",
)
def main(port, path):
    loghandler = logging.handlers.RotatingFileHandler(
        LOGFILE_NAME, maxBytes=MAX_LOGFILE_SIZE, backupCount=LOGFILE_BACKUP_COUNT
    )
    loghandler.setFormatter(logging.Formatter("%(asctime)s %(message)s"))
    logger = logging.Logger("ioc-exporter")
    logger.addHandler(loghandler)
    try:
        iocdirs = [
            os.path.join(path, f)
            for f in os.listdir(path)
            if os.path.isdir(os.path.join(path, f))
        ]
    except OSError:
        iocdirs = []
    REGISTRY.register(IOCCollector(iocdirs, logger))
    start_http_server(port)
    while True:
        time.sleep(1)


if __name__ == "__main__":
    main()
