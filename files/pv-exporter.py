"""
A Python2 Prometheus exporter for IOC PVs

This script sets up a webserver that exports to Prometheus data about PVs
"""

import logging
import logging.handlers
import os
import re
import time

import click
import yaml
from prometheus_client import start_http_server
from prometheus_client.core import REGISTRY, CounterMetricFamily, InfoMetricFamily
from pvmonitor import PVMonitor

# The iocStats property UPTIME is given in format "hh:mm:ss" with a conditional "x day(s), " prefix
UPTIME_REGEX = re.compile(r"(?P<day>\d+) days?, (?P<time>\d{2}:\d{2}:\d{2})")

IOC_CLONE_DIR = "/opt/iocs"
DEPLOYMENT_METADATA_FILE = ".deployment/metadata.yml"
LOGFILE_NAME = "/var/log/prometheus/pv-exporter.log"
MAX_LOGFILE_SIZE = 50 * 1024 * 1024  # 50MB
LOGFILE_BACKUP_COUNT = 1


class PropType:
    Counter = 1
    String = 2


PROPERTIES = {
    "UPTIME": PropType.Counter,
    "START_CNT": PropType.Counter,
    "HEARTBEAT": PropType.Counter,
    "BaseVersion": PropType.String,
    "require_VER": PropType.String,
}


class PVCollector(PVMonitor):
    def __init__(self, iocdirs, logger):
        super(PVCollector, self).__init__(logger)
        self.iocdirs = iocdirs

    def collect(self):
        info = InfoMetricFamily("pv_value", "Value of EPICS PV", labels=["ioc"])
        counter = CounterMetricFamily("pv_value", "Value of EPICS PV", labels=["pv"])

        for iocdir in self.iocdirs:
            try:
                with open(
                    os.path.join(os.path.abspath(iocdir), DEPLOYMENT_METADATA_FILE), "r"
                ) as config_file:
                    try:
                        ioc = yaml.safe_load(config_file)
                    except yaml.YAMLError as error:
                        self.logger.error(
                            "Failure loading config file {}: {}".format(
                                config_file, error
                            )
                        )
                        continue
                    else:
                        _run_dir = "/run/ioc@{}".format(os.path.basename(iocdir))
                        _iocname = ioc["name"]
                        if os.path.isdir(_run_dir):
                            self.subscribe_to_pvs(
                                [
                                    "{}:{}".format(_iocname, _property)
                                    for _property in PROPERTIES
                                ]
                            )
                        self.get_pvs(info, counter, _iocname)
            except Exception as error:
                self.logger.exception(error)
        yield info
        yield counter

    def get_pvs(self, metric_info, metric_counter, iocname):
        """Exports counters and info objects (pure labels) for select PVs."""
        for (prop, metric_type) in PROPERTIES.iteritems():
            val = self.get_pv("{}:{}".format(iocname, prop))
            if val is None:
                continue
            if metric_type == PropType.String:
                metric_info.add_metric([iocname], {prop: val})
            elif metric_type == PropType.Counter:
                if prop == "UPTIME":
                    match = UPTIME_REGEX.match(val)
                    if match:
                        day = int(match.group("day"))
                        hour, minute, second = map(int, match.group("time").split(":"))
                    else:
                        day = 0
                        hour, minute, second = map(int, val.split(":"))
                    val = second + minute * 60 + hour * 60 * 60 + day * 60 * 60 * 24
                metric_counter.add_metric(["{}:{}".format(iocname, prop)], val)


@click.command()
@click.option("--port", default=12111, help="TCP port the exporter will listen on")
@click.option(
    "--path",
    default=IOC_CLONE_DIR,
    type=click.Path(),
    help="Root directory for IOCs",
)
def main(port, path):
    loghandler = logging.handlers.RotatingFileHandler(
        LOGFILE_NAME, maxBytes=MAX_LOGFILE_SIZE, backupCount=LOGFILE_BACKUP_COUNT
    )
    loghandler.setFormatter(logging.Formatter("%(asctime)s %(message)s"))
    logger = logging.Logger("pv-exporter")
    logger.addHandler(loghandler)
    try:
        iocdirs = [
            os.path.join(path, f)
            for f in os.listdir(path)
            if os.path.isdir(os.path.join(path, f))
        ]
    except OSError:
        iocdirs = []
    REGISTRY.register(PVCollector(iocdirs, logger))
    start_http_server(port)
    while True:
        time.sleep(1)


if __name__ == "__main__":
    main()
